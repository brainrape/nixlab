# nix-shell environment for provisioning infrastructure and deployment

{pkgs ? import ./pinned-nixpkgs.nix {}, ...} :
pkgs.stdenv.mkDerivation {
  name = "nixlab-deploy-shell";
  buildInputs = [ pkgs.terraform pkgs.git pkgs.rsync ];
}
